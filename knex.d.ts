/// <reference path="../bluebird/bluebird.d.ts" />


interface KnexStatic {

    initialize(options: KnexInitializationOptions): Knex;

    //TODO: more...

}



interface KnexInitializationOptions {

    client: string;

    connection: Object;

    debug?: boolean;

    //TODO: more...

}



interface Knex {

    (tableName: string): KnexQueryBuilder;

    transaction: (callback: (trx: { commit: () => void; rollback: () => void; }) => void) => void;

    schema: KnexSchema;

    //TODO: more...

}

interface KnexQueryBuilder extends Promise<any[]> {
    select(): KnexQueryBuilder;
    select(...columns): KnexQueryBuilder
    count(columnName: string): KnexQueryBuilder;
    insert(row: Object): KnexQueryBuilder;
    insert(rows: Object[]): KnexQueryBuilder;
    where(options: Object): KnexQueryBuilder;
    andWhere(options: Object): KnexQueryBuilder;
    andWhere(columnName: string, operator: string, value: any): KnexQueryBuilder;
    where(columnName: string, operator: string, value: any): KnexQueryBuilder;
    whereIn(columnName: string, operator: string, value: any): KnexQueryBuilder;
    whereNotIn(columnName: string, values: any[]): KnexQueryBuilder;
    whereRaw(rawQuery: string): KnexQueryBuilder;
    update(columnName: string, value: string): KnexQueryBuilder;
    update(columns: Object): KnexQueryBuilder;
    transacting(transaction: any): KnexQueryBuilder;
    offset(amount: number): KnexQueryBuilder;
    limit(amount: number): KnexQueryBuilder;
    //TODO: more...

}



interface KnexSchema {

    createTable(tableName: string, callback: (table: KnexTableBuilder) => void): Promise<{}>;

    hasColumn(tableName: string, columnName: string): Promise<boolean>;

    hasTable(tableName: string): Promise<boolean>;

    table(tableName: string, callback: (table: KnexTableBuilder) => void): Promise<{}>;

    //TODO: more...

}



interface KnexTableBuilder {

    increments(columnName?: string): KnexColumnBuilder;

    integer(columnName: string): KnexColumnBuilder;

    bigInteger(columnName: string): KnexColumnBuilder;

    text(columnName: string, textType?: string): KnexColumnBuilder;

    date(columnName: string): KnexColumnBuilder;

    dateTime(columnName: string): KnexColumnBuilder;

    //TODO: more...

}



interface KnexColumnBuilder {

    defaultTo(value: any): KnexColumnBuilder;

    notNullable(): KnexColumnBuilder;

    nullable(): KnexColumnBuilder;

    primary(): KnexColumnBuilder;

    index(indexName?: string, indexType?: string): KnexColumnBuilder;

    references(columnName: string): KnexForeignKeyBuilder;

    //TODO: more...

}



interface KnexForeignKeyBuilder {

    inTable(tableName: string): KnexColumnBuilder;

}



declare module "knex" {

    var _: KnexStatic;

    export = _;

}